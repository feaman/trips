<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Detail */

$this->title = 'Изменение точки';
$this->params['breadcrumbs'][] = ['label' => $model->trip->title, 'url' => ['/site/view?id=' . $model->trip->id]];
$this->params['breadcrumbs'][] = 'Изменение точки';
?>
<div class="detail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
