<?php

use app\assets\AppAsset;
use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Trip */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Путешествия', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js\lightbox2\dist\js\lightbox.min.js', ['depends' => AppAsset::className()]);
$this->registerCssFile('/js\lightbox2\dist\css\lightbox.min.css');
/** @var User $currentUser */
$currentUser = Yii::$app->user->identity;
?>
<div class="trip-view">

    <h1><?= $this->title ?></h1>
    <h4><?= $model->introduction ?></h4>

    <?php $stepNumber = 1; foreach ($model->details as $tripStep) { ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Точка <strong>#<?= $stepNumber ?></strong></h3>
                <?php
                    if ($currentUser->nick_name === 'Elf') {
                        echo Html::a('Изменить точку', ['/detail/update?id=' . $tripStep->id], ['class' => 'btn btn-success']);
                        if ($model->hidden) {
                            echo Html::a('Вернуть взад точку', ['/detail/restore?id=' . $tripStep->id], ['class' => 'btn btn-danger']);
                        } else {
                            echo Html::a('Скрыть точку', ['/detail/delete?id=' . $tripStep->id], ['class' => 'btn btn-danger']);
                        }
                    }
                ?>
            </div>
            <div class="panel-body">
                <a href="<?= '/photos/medium/' . $tripStep->photo ?>" data-lightbox="<?= $tripStep->id ?>">
                    <?= Html::img('/photos/small/' . $tripStep->photo, ['class' => 'img-responsive']) ?>
                </a>
            </div>
            <?php if ($tripStep->text) { ?>
                <div class="panel-footer"><?= $tripStep->text ?></div>
            <?php } ?>
        </div>
    <?php $stepNumber++; } ?>

    <?php $comments = $model->comments; ?>
    <?php if ($comments) { ?>
    <h3 class="text-center"><strong>Комментарии</strong></h3>
    <?php foreach ($model->comments as $comment) { ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <strong><?= $comment->user->nick_name ?></strong>
                    <span class="text-muted"><?= date('d.m.Y H:i:s', strtotime($comment->date)) ?></span>
                </h3>
            </div>
            <div class="panel-body">
                <?= $comment->text ?>
            </div>
        </div>
    <?php }} ?>

</div>
