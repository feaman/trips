<?php

use app\models\Trip;
use app\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TripSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model Trip */

$this->title = 'Путешествия';
$this->params['breadcrumbs'][] = $this->title;
/** @var User $currentUser */
$currentUser = Yii::$app->user->identity;
?>
<div class="trip-index">

    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if ($currentUser->nick_name === 'Elf') { ?>
        <p>
            <?= Html::a('Добавить сказание', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>

    <div class="container">
        <?php foreach ($dataProvider->getModels() as $model) { ?>
            <div class="trip__title">
                <a href="/site/view?id=<?= $model->id ?>" class="trip__title-link">
                    <div>
                        <strong><?= $model->title . ($model->hidden ? ' (скрыто)' : '') ?></strong>
                    </div>
                    <div>
                        <?php if ($model->distance) { ?>
                            <?= $model->distance ?> км
                        <?php } ?>
                        <?php if ($model->time) { ?>
                            , <?= $model->time ?> часа(ов)
                        <?php } ?>
                    </div>
                    <?php
                        if ($currentUser->nick_name === 'Elf') {
                            echo Html::a('Изменить сказание', ['update?id=' . $model->id], ['class' => 'btn btn-success']);
                            if ($model->hidden) {
                                echo Html::a('Вернуть взад сказание', ['restore?id=' . $model->id], ['class' => 'btn btn-danger']);
                            } else {
                                echo Html::a('Скрыть сказание', ['delete?id=' . $model->id], ['class' => 'btn btn-danger']);
                            }
                        }
                    ?>
                </a>
            </div>
        <?php } ?>
    </div>


</div>
