<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trip".
 *
 * @property int $id
 * @property string $link
 * @property string $title
 * @property int $distance
 * @property int $time
 * @property int $num
 * @property string $introduction
 * @property string $date
 * @property int $hidden
 *
 * @property Detail[] $details
 * @property Comment[] $comments
 */
class Trip extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['distance', 'time', 'num', 'hidden'], 'integer'],

            [['introduction'], 'string'],
            [['introduction'], 'replaceNlToBr'],

            [['date'], 'safe'],

            [['link', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'title' => 'Название',
            'distance' => 'Дистанция',
            'time' => 'Время',
            'num' => 'Номер по порядку',
            'introduction' => 'Вступление',
            'date' => 'Дата',
            'hidden' => 'Скрытый',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetails()
    {
        return $this->hasMany(Detail::className(), ['trip_id' => 'id'])->orderBy(['num' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['trip_id' => 'id']);
    }
}
