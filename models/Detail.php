<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "detail".
 *
 * @property int $id
 * @property int $trip_id
 * @property int $num
 * @property string $text
 * @property string $photo
 * @property string $date
 *
 * @property Trip $trip
 */
class Detail extends BaseActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trip_id', 'num'], 'integer'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['photo'], 'string', 'max' => 255],
            [['trip_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trip::className(), 'targetAttribute' => ['trip_id' => 'id']],
            [['imageFile'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'trip_id' => 'Trip ID',
            'num' => 'Num',
            'text' => 'Текст',
            'photo' => 'Картинка',
            'date' => 'Date',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->imageFile) {
            $fileName = 'journey_' . $this->trip->id . '_(' . $this->id . ').' . $this->imageFile->extension;
            $this->imageFile->saveAs('photos/medium/' . $fileName, false);
            $this->imageFile->saveAs('photos/small/' . $fileName);
            $this->imageFile = null;
            $this->photo = $fileName;
            $this->save(false);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrip()
    {
        return $this->hasOne(Trip::className(), ['id' => 'trip_id']);
    }
}
