<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $nick_name
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $from
 * @property string $email
 * @property string $reg_date
 *
 * @property Comment[] $comments
 */
class User extends BaseActiveRecord implements \yii\web\IdentityInterface
{
    public $auth_key;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reg_date'], 'safe'],
            [['nick_name', 'password', 'first_name', 'last_name', 'from'], 'string', 'max' => 55],
            [['email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nick_name' => 'Nick Name',
            'password' => 'Password',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'from' => 'From',
            'email' => 'Email',
            'reg_date' => 'Reg Date',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function findByUsername(string $userName)
    {
        return static::findOne(['nick_name' => $userName]);
    }

    public function validatePassword(string $password)
    {
        return $this->password === $password;
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['user_id' => 'id']);
    }
}
