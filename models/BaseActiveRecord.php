<?php

namespace app\models;

class BaseActiveRecord extends \yii\db\ActiveRecord
{
    public function replaceNlToBr(string $attribute)
    {
        $this->$attribute = nl2br($this->$attribute);
    }
}
