<?php

namespace app\components;

use app\models\User;
use Yii;
use yii\base\UserException;
use yii\web\Controller;

class BaseController extends Controller
{
    public function checkRights()
    {
        /** @var User $currentUser */
        $currentUser = Yii::$app->user->identity;
        if ($currentUser->nick_name !== 'Elf') {
            throw new UserException('Нет прав');
        }
    }
}
